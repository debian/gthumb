Source: gthumb
Section: gnome
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 appstream,
 bison,
 debhelper-compat (= 13),
 flex,
 gsettings-desktop-schemas-dev,
 itstool,
 libappstream-dev,
 libbrasero-media3-dev,
 libcolord-dev,
 libexiv2-dev,
 libgstreamer-plugins-base1.0-dev,
 libgstreamer1.0-dev,
 libgtk-3-dev,
 libheif-dev (>= 1.11~),
 libjxl-dev,
 libraw-dev,
 librsvg2-dev,
 libsecret-1-dev,
 libtiff-dev,
 libwebp-dev,
 libx11-dev,
 meson
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/gthumb.git
Vcs-Browser: https://salsa.debian.org/debian/gthumb
Homepage: https://wiki.gnome.org/Apps/Gthumb
Rules-Requires-Root: no

Package: gthumb
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gsettings-desktop-schemas,
 gthumb-data (= ${source:Version}),
 libgl1-mesa-dri,
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 gthumb2,
Provides:
 gthumb2,
Breaks:
 gthumb-data (<< 3:3.4.4.1-4~),
Replaces:
 gthumb-data (<< 3:3.4.4.1-4~),
Description: image viewer and browser
 gThumb is an advanced image viewer and browser. It has many useful
 features, such as filesystem browsing, slide show, image catalogs, web
 album creation, camera import, image CD burning, batch file operations and
 quick image editing features like transformation and color manipulation.
 .
 It's designed for GNOME desktop environment and uses its platform. For
 camera import feature, the gPhoto2 library is used.

Package: gthumb-data
Architecture: all
Breaks:
 gthumb (<< 3:2.10.8-1),
Replaces:
 gthumb (<< 3:2.10.8-1),
Recommends:
 yelp,
Depends:
 ${misc:Depends},
Description: image viewer and browser - arch-independent files
 gThumb is an advanced image viewer and browser. It has many useful
 features, such as filesystem browsing, slide show, image catalogs, web
 album creation, camera import, image CD burning, batch file operations and
 quick image editing features like transformation and color manipulation.
 .
 It's designed for GNOME desktop environment and uses its platform. For
 camera import feature, the gPhoto2 library is used.
 .
 This package contains the architecture-independent files needed by gthumb.

Package: gthumb-dev
Architecture: any
Multi-Arch: no
Section: devel
Depends:
 gthumb (= ${binary:Version}),
 ${misc:Depends},
Description: image viewer and browser - development files
 gThumb is an advanced image viewer and browser. It has many useful
 features, such as filesystem browsing, slide show, image catalogs, web
 album creation, camera import, image CD burning, batch file operations and
 quick image editing features like transformation and color manipulation.
 .
 It's designed for GNOME desktop environment and uses its platform. For
 camera import feature, the gPhoto2 library is used.
 .
 This package contains the files needed to develop third-party extensions.
